//import com.zuitt.example.Car;
import  com.zuitt.example.*;//para ma access lahat nung nasa package "*" Universal
public class Main {
    public static void main(String[] args) {

//        System.out.println("Hello world!");

        //OOP
            //Stands for "Object-Oriented Programming"
            // OOP is a programming model that allows developers to design software around data or objects, rather than function and logic

        //OOP Concepts
            //Objects - abstract idea that represents something in the real world
            //Class(blueprint) - representation of the objects using code
            //Instance - unique copy of the idea, made "physical".

        //Objects
            //States and Attributes - what is the idea about
            //Behaviour - what can idea do?
            //Example: A person has attributes like name, age, height and weight, And (behavior) a person can eat, sleep and speak

        //Four Pillar of OOP
        //1. Encapsulation
            //data hiding - the variables of a class will be hidden from other classes and can be accessed only through the methods of the current class(may magact as a middleman para ma access yung data)
            //variables/properties as private.
            //provide a public setter and getter function

        //Create a Car
        Car myCar = new Car();
        myCar.drive();

        //Assign properties of myCar using the setter methods
        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2025);

        // to view the properties of myCar using the getter methods
        System.out.println("Car name: " + myCar.getName());
        System.out.println("Car brand: " + myCar.getBrand());
        System.out.println("Car year of make: " + myCar.getYearOfMake());

        //Composition and Inheritance
            //Both Concepts which promotes code reuse through a different approach
            //"Inheritance" allows modelling an object that is a subset of another objects
            //It defines "is a relationship"
            //To design a class on what it is

            //"Composition" allows modelling an object that are made up of other objects
                //both entities are dependent on each other
                //Composed object cannot exist without the other entity
                //It defines "has a relationship"
                // To design a class on what it does
                //Example:
                    //A car is a vehicle - inheritance
                    //A car has a driver - composition
        System.out.println("Driver name: " + myCar.getDriverName());
        myCar.setDriver("John Smith");
        System.out.println("Driver name: " + myCar.getDriverName());

            //2. Inheritance
            // can be defined as the process where one class acquires the properties and methods of another class
            // With the use of inheritance, the information is made manageable in hierarchical order.

        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        myPet.call();//priority yung child class natatawig den yung parent class

        System.out.println("Pet name: " + myPet.getName() + ". Breed: " + myPet.getBreed() + ". Color: " + myPet.getColor());

        //3. Abstraction
        // is a process where all the logic and complexity are hidden from the user.
        //The user would know what to do rather than how it is done.
            //Interfaces
                //This is used to achieve total abstraction
                //Creating Abstract classes doesn't support "multiple inheritance", but it can be achieve with interfaces
                // act as "contracts" wherein a class implements the interfaces should have the methods that the interface has defined in the class.
        Person child = new Person();
        child.sleep();
        child.run();
        child.morningGreet();
        child.holidayGreet();

        //4. Polymorphism
        // Derived from greek word: poly means "many" and morph means "forms".
        // In short "many forms".
        // Polymorphism is the ability of an object to take on many forms.
        //This is usually done by function/method overloading/overriding
        //May original class/method
        //from original class/method binigayan natn ng ibang work

        //Two main types of Polymorphism
            //Static or Compile time polymorphism -(We are checking the syntax or the rules of java)

            //methods with the same name, but they have different data types and a different number of arguments
            //kahit mag kakaparehas yung pangalan ng method basta mag kakaiba yung Argument And yung Datatypes
        //constructor overloading
        StaticPoly myAddition = new StaticPoly();
        //Original Method
        System.out.println(myAddition.addition(5,6));
        //based on additional arguments
        System.out.println(myAddition.addition(5,6,7));
        //based on changing data types
        System.out.println(myAddition.addition(5.5,6.6));

            //2. Dynamic or Run-time Polymorphism
            //Function is overriden by replacing the definition of the method in the parent class to the child class
            //same method name but babaguhin yung ginagawa ni method sa orginal na work
            //Overriding
            /*
                Parent Class                                Child
                    name                                    name
                    address                                 Address
                                                     ->
                    work("I am a Developer")                 work("I am manager")


            */
            Child myChild = new Child();
            myChild.speak();



    }
}