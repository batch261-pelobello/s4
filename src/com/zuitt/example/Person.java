package com.zuitt.example;

//Interface Implementation
//This is where we implement our Action interface
public class Person implements Actions, Greetings {
    //dito mag iimplement yung mga ginagawa nung abstract or yung interface
    //dito nilalagay yung mga logic ng mga inimplement the interfaces

    public void sleep() {
        System.out.println("ZZZZZzzzzzz...");
    }

    public void run() {
        System.out.println("Running...");
    }


    public void morningGreet() {
        System.out.println("Good morning!");
    }

    public void holidayGreet() {
        System.out.println("Happy Holidays!");
    }
}
