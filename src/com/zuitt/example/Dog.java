package com.zuitt.example;
//Child class of Animal
    //"extends" keywird us used to inherit the properties and methods of the parent class
public class Dog extends Animal{
        //Inheritance
        //properties
        //exclusive lang kay Dog na mga properties

    private String breed;

    //constructor

    public Dog(){
        //super() refer to the immediate parent class
        //to have a direct acces with the original constructor(Parent Class)
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name,color);
        this.breed= breed;
    }

    //Getter and Setter

    public String getBreed(){
        return this.breed;
    }
    public void setBreed(String breed){
        this.breed = breed;
    }

    //method
    public void speak(){
        System.out.println("Woof, woof");
    }
    public void call(){
        super.call();
        System.out.println("Hi! My name is " + this.getName() + ", I am a dog.");
    }
}
