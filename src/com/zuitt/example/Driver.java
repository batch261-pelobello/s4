package com.zuitt.example;

public class Driver {
    //properties/variables
    //Composition
    private String name;

    public Driver(String name) {
        this.name=name;
    }


    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name=name;
    }
}
